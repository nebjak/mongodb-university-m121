var pipeline = [
  {
    $match: {
      'imdb.rating': { $gt: 0 },
      metacritic: { $gt: 0 }
    }
  },
  {
    $facet: {
      tomImdb: [
        {
          $sort: { 'imdb.rating': -1 }
        },
        { $limit: 10 }
      ],
      topMetacritic: [
        {
          $sort: { metacritic: -1 }
        },
        { $limit: 10 }
      ]
    }
  },
  {
    $project: {
      commonTopMovies: {
        $size: {
          $setIntersection: ['$tomImdb', '$topMetacritic']
        }
      }
    }
  }
];
