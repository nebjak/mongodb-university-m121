var pipeline = [
  {
    $match: {
      languages: 'English',
      'imdb.rating': { $gte: 1 },
      'imdb.votes': { $gte: 1 },
      year: { $gte: 1990 }
    }
  },
  {
    $addFields: {
      scaled_votes: {
        $add: [
          1,
          {
            $multiply: [
              9,
              {
                $divide: [
                  { $subtract: ['$imdb.votes', 5] },
                  { $subtract: [1521105, 5] }
                ]
              }
            ]
          }
        ]
      }
    }
  },
  {
    $addFields: {
      normalized_rating: {
        $avg: ['$scaled_votes', '$imdb.rating']
      }
    }
  },
  {
    $sort: {
      normalized_rating: 1
    }
  },
  {
    $project: {
      _id: 0,
      title: 1
    }
  },
  {
    $limit: 1
  }
];
