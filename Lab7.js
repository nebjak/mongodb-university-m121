var pipeline = [
  { $unwind: '$airlines' },
  {
    $lookup: {
      from: 'air_routes',
      localField: 'airlines',
      foreignField: 'airline.name',
      as: 'routes'
    }
  },
  { $unwind: '$routes' },
  { $match: { 'routes.airplane': { $in: ['747', '380'] } } },
  {
    $group: {
      _id: '$name',
      routes_count: { $sum: 1 }
    }
  },
  { $sort: { routes_count: -1 } }
];

//alterantive solution

var pipeline2 = [
  {
    $match: {
      airplane: {
        $in: ['747', '380']
      }
    }
  },
  {
    $lookup: {
      from: 'air_airlines',
      localField: 'airline.name',
      foreignField: 'name',
      as: 'airline'
    }
  },
  {
    $lookup: {
      from: 'air_alliances',
      localField: 'airline.name',
      foreignField: 'airlines',
      as: 'air_alliances'
    }
  },
  {
    $group: {
      _id: '$air_alliances.name',
      count: {
        $sum: 1
      }
    }
  },
  {
    $sort: {
      count: -1
    }
  }
];
